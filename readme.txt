Prosty generator sieci spo�ecznej

Instrukcja:

1. W pliku "nodes" nale�y wpisa� list� imion i nazwisk (b�d� to w�z�y sieci)
2. Nale�y uruchomic pomocniczy skrypt "numbers.py", kt�ry ponumeruje osoby wpisane w pliku "nodes". Wynik zostanie zapisany do    "nodes1".
3. Nast�pnie, nale�y uruchomi� skrypt "friends_match.py", kt�ry losowo dobierze osoby z pliku "nodes1" w pary. B�d� to kraw�dzie grafu. Wynik b�dzie zapisany do pliku "edges1"
4. Nast�pnie, aby pozby� si� duplikat�w znajomych, nale�y uruchomi� skrypt "remove_dupicates.py". Wyniki zostan� zapisane do "edges_ready".
5.Do wizualizacji sieci s�u�y skrypt "graph.py".
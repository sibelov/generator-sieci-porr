import networkx as nx
import matplotlib.pyplot as plt

my_dict = {}
with open("nodes1") as nodes:
    for line in nodes:
        key, value = line.split(":")
        my_dict[int(key)] = value.strip()

#print(my_dict)

my_graph= nx.read_edgelist("edges_ready", create_using=nx.Graph(), nodetype=int)
d = nx.degree(my_graph)

my_graph = nx.relabel_nodes(my_graph, my_dict)

pos = nx.spring_layout(my_graph) #lepsze ułożenie węzłów grafu
print(list(d))

#nx.draw(my_graph, pos, node_size=[v * 100 for v in d.value()], with_labels=True)
nx.draw(my_graph, pos, with_labels=True)
plt.show()
print(nx.info(my_graph))
